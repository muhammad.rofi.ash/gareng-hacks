FROM containers.cisco.com/nso-docker/cisco-nso-dev:5.7.8

# Define working directory.
WORKDIR $NCS_RUN_DIR

# Define username and password for NSO
ENV ADMIN_USERNAME=admin
ENV ADMIN_PASSWORD=admin
ENV CISCO_IOS_XE_NED=ncs-5.7-cisco-ios-6.77.7.tar.gz
ENV CISCO_IOS_XR_NED=ncs-5.7.8-cisco-iosxr-7.44.2.tar.gz

# Copy ned(s) and internal package(s)
COPY docker/ncs.conf /etc/ncs/ncs.conf
COPY external/ncs-5.7.8-cisco-iosxr-7.44.2.tar.gz ./nso/run/packages/
COPY external/ncs-5.7-cisco-ios-6.77.7.tar.gz ./nso/run/packages/

# Copy Post NCS start scripts
COPY ./docker/add-default-authgroups.sh /etc/ncs/post-ncs-start.d
RUN chmod 777 /etc/ncs/post-ncs-start.d/add-default-authgroups.sh