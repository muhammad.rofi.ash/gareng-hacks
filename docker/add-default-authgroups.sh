NCS="ncs_cli -C -u admin"

echo """
config
devices authgroups group default
commit
""" | $NCS

echo """
config
devices authgroups group default umap admin remote-name admin remote-password admin remote-secondary-password admin
commit
""" | $NCS